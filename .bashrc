#
# ~/.bashrc
#
export MOZ_ENABLE_WAYLAND=1
#export QT_STYLE_OVERRIDE=kvantum
#export QT_QPA_PLATFORM=wayland
export JDK_JAVA_OPTIONS='-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel'
export BROWSER=/usr/bin/firefox
GPG_TTY=$(tty)
export GPG_TTY
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

fortune -ac

## verbos prompt j: jobs {time} e: error status of the last command h: history no
# !(history no) exucutes that command. To illustrate !21 executes the command
# with history no 21. To see history run history

export PS1='\nj(\j) {\t} e($?) h(\!) [\w]\$ '

hash df du lsd pkgfile dust tldr

# Uncomment the following line if you don't like systemctl's auto-paging feature
# export SYSTEMD_PAGER=

# User specific aliases and functions

shopt -s globstar # make filename expansion recursive with '**'syntax
shopt -s cdspell # correct spelling for cd in interactive shell
shopt -s extglob # extended glob
shopt -s autocd ## autocd: if the input is directory name, bash changes directory to that input
shopt -s checkwinsize
shopt -s dirspell # correct directory names in interactive shell

# Verbose ls
alias ll='ls -lsAFh --hyperlink=auto --time-style=iso --color=always '
alias l='lsd -AhFl --total-size --group-dirs first --icon-theme=fancy '

alias df='df -h' # human readable df
alias du='du -chs' # current folders size or with argument such as `du * `
# clear screen for real (it does not work in Terminology)
alias cls=' echo -ne "\033c"'
alias cp="cp -ia"   # confirm before overwriting something
alias free='free -h' # show sizes in MB

## Colourful output for diff and grep by default
alias diff='diff --color=auto'
alias grep='grep --colour=auto'
alias egrep='egrep --colour=auto'
alias fgrep='fgrep --colour=auto'
export GCC_COLOR='error=01;31:warning=01;35:note=01;36:range1=32:range2=34:locus=01:\quote=01:path=01;36:fixit-insert=32:fixit-delete=31:\diff-filename=01:diff-hunk=32:diff-delete=31:diff-insert=32:\type-diff=01;32'
## Backwards compatible more convenient 'mkdir'
alias mkdir='mkdir -p -v '

## 'dd' command with progress bar by default
alias dd='dd status=progress '

## modern replacement of cp with progressbar.
alias rs='rsync -auh --info=progress2 '
# Download from youtube or youtube music 
alias you='youtube-dl --geo-bypass -wc --audio-quality 256K --audio-format mp3 --embed-thumbnail -x --add-metadata -o "%(artist)s - %(track)s.%(ext)s"'

alias less='less --no-histdups --follow-name -iJKMNQFwsz-2 ' # uncomment to replace less with

## jvm font fix. If java programmes such as netbeans have distorted fonts uncomment this
# export _java_options='-dawt.usesystemaafontsettings=lcd -dswing.aatext=true'

## stop logging of repeated identical commands 
export HISTCONTROL=ignoreboth

## when command not found, search it in the repositories by the package pkgfile
source /usr/share/doc/pkgfile/command-not-found.bash

## Change directory verbose version. When you change directory
# it also lists the contents. In addition it keeps a history and shows this
# history at the beginning of the output and you can surf the history with
# +N or -N N is a digit
# example:
# history is like this: ~ ~/Documents/ /usr/bin/ /sys/ ~
# That means you are at ~
# to return to /sys write `cv +3` counting from right or
# `cv -1` counting from left

cv() {
	local dir="$1"
	local dir="${dir:=$HOME}"
	if [[ -d "$dir" || "$dir" == [+-][0-9] ]]; then
       	    pushd "$dir" && l
	else
	    echo "bash: cv: $dir: directory not found"
	fi
}

## Learn your public ip, for internal ip
pubip(){
    curl ipinfo.io
}

## Create a temporary folder in the ram perfect for fast read/write such as
# compilation of cloned source code
tmp() {
	cd $(mktemp -d)
}

